package be.test.feignmockbean;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class SpringService {

    private final SpringClient springClient;

    public String getContent() {
        return springClient.getPage();
    }

}
