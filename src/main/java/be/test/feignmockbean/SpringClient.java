package be.test.feignmockbean;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

@FeignClient(name = "spring", url = "https://spring.io")
public interface SpringClient {

    @GetMapping("/")
    String getPage();

}
