package be.test.feignmockbean;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients(clients = SpringClient.class)
public class FeignMockBeanApplication {

    public static void main(String[] args) {
        SpringApplication.run(FeignMockBeanApplication.class, args);
    }

}
