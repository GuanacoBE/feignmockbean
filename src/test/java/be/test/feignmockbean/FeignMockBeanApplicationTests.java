package be.test.feignmockbean;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import static org.mockito.Mockito.when;

@SpringBootTest
class FeignMockBeanApplicationTests {

    //@MockBean(name = "be.test.feignmockbean.SpringClient") // 2.2.7
    @MockBean // 2.2.6
    private SpringClient springClient;

    @Autowired
    private SpringService springService;

    @Test
    void contextLoads() {
        when(springClient.getPage())
                .thenReturn("MOCK PAGE");

        var content = springService.getContent();

        Assertions.assertThat(content).isEqualTo("MOCK PAGE");

    }

}
